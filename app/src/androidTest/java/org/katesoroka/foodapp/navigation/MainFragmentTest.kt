package org.katesoroka.foodapp.navigation

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith
import org.katesoroka.foodapp.R
import org.katesoroka.foodapp.view.main.FoodViewHolder
import org.katesoroka.foodapp.view.main.MainFragment
import org.mockito.Mockito.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@RunWith(AndroidJUnit4::class)
class MainFragmentTest {

    var latch = CountDownLatch(1)

    @Test
    fun testNavigationToProductFragment() {
        val mockNavController = mock(NavController::class.java)

        val foodScenario = launchFragmentInContainer<MainFragment>(
            null, R.style.AppTheme_NoActionBar)

        foodScenario.onFragment { fragment ->
            Navigation.setViewNavController(fragment.requireView(), mockNavController)
        }

        latch.await(2000, TimeUnit.MILLISECONDS)

        onView(ViewMatchers.withId(R.id.foodListView))
            .perform(RecyclerViewActions
                .actionOnItemAtPosition<FoodViewHolder>(0, ViewActions.click()))

        verify(mockNavController).navigate(R.id.action_mainFragment_to_productActivity)
    }

}