package org.katesoroka.foodapp.database

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.katesoroka.foodapp.data.database.AppDatabase
import org.katesoroka.foodapp.data.database.ProductDao
import org.katesoroka.foodapp.data.model.Food
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class FoodDBTest {

    private lateinit var database: AppDatabase

    private lateinit var productDao: ProductDao

    private val testFoodList = listOf(Food("id0", "name0", 0, "imageUrl0"),
        Food("id1", "name1", 1, "imageUrl1"),
        Food("id2", "name2", 2, "imageUrl2"),
        Food("id3", "name3", 3, "imageUrl3"))

    @Before
    fun createDatabase() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(
                context, AppDatabase::class.java).build()
        productDao = database.productDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDatabase() {
        database.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeAndReadProduct() {
        val expectedProduct = testFoodList[0]
        productDao.insertAll(expectedProduct)
        productDao.findAll()
            .map { it[0] }
            .test()
            .assertSubscribed()
            .assertValues(expectedProduct)
            .assertComplete()
            .assertNoErrors()
    }

    @Test
    @Throws(Exception::class)
    fun writeAndReadProducts() {
        val expectedProducts = testFoodList
        productDao.insertAll(*expectedProducts.toTypedArray())

        productDao.findAll()
            .test()
            .assertSubscribed()
            .assertValues(expectedProducts)
            .assertComplete()
            .assertNoErrors()
    }

    @Test
    @Throws(Exception::class)
    fun deleteProducts() {
        val allCards = testFoodList
        productDao.insertAll(*allCards.toTypedArray())
        productDao.deleteAll()
        productDao.findAll()
            .test()
            .assertSubscribed()
            .assertValues(emptyList())
            .assertComplete()
            .assertNoErrors()
    }

}