package org.katesoroka.foodapp.database

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.katesoroka.foodapp.data.database.AppDatabase
import org.katesoroka.foodapp.data.database.ProductDao
import org.katesoroka.foodapp.data.model.Food
import org.katesoroka.foodapp.data.model.FoodFull
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class FoodFullDBTest {

    private lateinit var database: AppDatabase

    private lateinit var productDao: ProductDao

    private val testFoodList = listOf(
        Food("id0", "name0", 0, "imageUrl0"),
        Food("id1", "name1", 1, "imageUrl1"),
        Food("id2", "name2", 2, "imageUrl2"),
        Food("id3", "name3", 3, "imageUrl3")
    )

    private val testFoodFullList = listOf(
        FoodFull("id0", "name0", 0, "imageUrl0", "description0"),
        FoodFull("id1", "name1", 1, "imageUrl1", "description1"),
        FoodFull("id2", "name2", 2, "imageUrl2", "description2"),
        FoodFull("id3", "name3", 3, "imageUrl3", "description3"))

    @Before
    fun createDatabase() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(
                context, AppDatabase::class.java).build()
        productDao = database.productDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDatabase() {
        database.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeAndFindProduct() {
        val expectedProduct = testFoodFullList[2]
        productDao.insertAll(*testFoodList.toTypedArray())
        productDao.insertProduct(testFoodFullList[0])
        productDao.insertProduct(testFoodFullList[1])
        productDao.insertProduct(testFoodFullList[2])
        productDao.insertProduct(testFoodFullList[3])

        productDao.findProductById(expectedProduct.id)
            .test()
            .assertSubscribed()
            .assertValues(expectedProduct)
            .assertComplete()
            .assertNoErrors()
    }

}