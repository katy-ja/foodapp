package org.katesoroka.foodapp

import android.app.Application
import org.katesoroka.foodapp.di.*
import org.koin.android.ext.android.startKoin

class FoodApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(activityModule, storeModule, networkModule))
    }

}