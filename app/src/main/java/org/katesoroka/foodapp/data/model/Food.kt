package org.katesoroka.foodapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

// todo There should be different models for network and for db
@Entity(tableName = "food")
data class Food(
    @PrimaryKey @SerializedName("product_id") val id: String = "",
    @SerializedName("name") val name: String = "",
    @SerializedName("price") val price: Long = 0L,
    @SerializedName("image") val imageUrl: String = ""
)