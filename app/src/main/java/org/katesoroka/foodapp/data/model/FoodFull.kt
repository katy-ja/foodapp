package org.katesoroka.foodapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "food_full")
data class FoodFull(
    @PrimaryKey @SerializedName("product_id") val id: String = "",
    @SerializedName("name") val name: String = "",
    @SerializedName("price") val price: Long = 0L,
    @SerializedName("image") val imageUrl: String = "",
    @SerializedName("description") val description: String = ""
)