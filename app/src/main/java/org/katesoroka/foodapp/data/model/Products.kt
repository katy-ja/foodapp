package org.katesoroka.foodapp.data.model

import com.google.gson.annotations.SerializedName

data class Products(@SerializedName("products") val products: Array<Food>) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Products

        if (!products.contentEquals(other.products)) return false

        return true
    }

    override fun hashCode(): Int {
        return products.contentHashCode()
    }

}