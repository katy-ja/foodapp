package org.katesoroka.foodapp.data.repo

import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import org.katesoroka.foodapp.data.model.Food
import org.katesoroka.foodapp.data.api.FoodApi
import org.katesoroka.foodapp.data.database.ProductDao
import org.katesoroka.foodapp.data.model.FoodFull
import org.katesoroka.foodapp.data.model.Products

class FoodRepositoryImpl(
    private val api: FoodApi,
    private val productDao: ProductDao
) : FoodRepository {

    override fun getProducts(): Flowable<List<Food>> {
        return Flowable.merge(
            productDao.findAll(),
            api.getProducts()
                .onErrorReturn { Products(emptyArray()) }
                .filter { products -> products.products.isNotEmpty() }
                .map { products -> products.products }
                .flatMap { products ->
                    productDao.deleteAll()
                    productDao.insertAll(*products)
                    return@flatMap Flowable.just(emptyList<Food>())
                }
                .filter { products -> products.isNotEmpty() }
        )
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
    }

    override fun getProduct(id: String): Flowable<FoodFull> {
        return Flowable.merge(
            productDao.findProductById(id),
            api.getProduct(id)
                .onErrorReturn { FoodFull() }
                .filter { product -> product.id.isNotEmpty() }
                .flatMap { product ->
                    productDao.insertProduct(product)
                    return@flatMap Flowable.just(product)
                }
//                .compose { exceptionHandling(it) }
        )
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
    }

//    private fun <T>exceptionHandling(observable: Flowable<T>): Flowable<T> {
//        return observable
//            .doOnError {  }
//            .retry()
//    }

}