package org.katesoroka.foodapp.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import org.katesoroka.foodapp.data.model.Food
import org.katesoroka.foodapp.data.model.FoodFull

@Database(entities = [
    Food::class,
    FoodFull::class
], version = 1)
abstract class AppDatabase : RoomDatabase() {

    companion object {

        const val DATABASE_NAME = "card_database"

    }

    abstract fun productDao(): ProductDao

}