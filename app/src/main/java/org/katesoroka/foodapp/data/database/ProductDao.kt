package org.katesoroka.foodapp.data.database

import androidx.room.*
import io.reactivex.Flowable
import org.katesoroka.foodapp.data.model.Food
import org.katesoroka.foodapp.data.model.FoodFull

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg products: Food): List<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProduct(product: FoodFull): Long

    @Query("SELECT * FROM food")
    fun findAll(): Flowable<List<Food>>

    @Query("SELECT * FROM food_full WHERE id = :id LIMIT 1")
    fun findProductById(id: String): Flowable<FoodFull>

    @Query("DELETE FROM food")
    fun deleteAll()

}