package org.katesoroka.foodapp.data.api

import io.reactivex.Flowable
import org.katesoroka.foodapp.data.model.FoodFull
import org.katesoroka.foodapp.data.model.Products
import retrofit2.http.GET
import retrofit2.http.Path

interface FoodApi {

    @GET("list")
    fun getProducts(): Flowable<Products>

    @GET("{product_id}/detail")
    fun getProduct(@Path("product_id") id : String): Flowable<FoodFull>

}