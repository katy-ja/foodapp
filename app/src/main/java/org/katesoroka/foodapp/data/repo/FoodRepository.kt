package org.katesoroka.foodapp.data.repo

import io.reactivex.Flowable
import org.katesoroka.foodapp.data.model.Food
import org.katesoroka.foodapp.data.model.FoodFull

interface FoodRepository {

    fun getProducts(): Flowable<List<Food>>

    fun getProduct(id : String): Flowable<FoodFull>

}