package org.katesoroka.foodapp.di

import org.katesoroka.foodapp.view.details.ProductViewModel
import org.katesoroka.foodapp.view.main.MainViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val activityModule = module {

//    viewModel<MainViewModel>()

    viewModel { MainViewModel(get()) }

    viewModel { (productId : String) -> ProductViewModel(productId, get()) }

}