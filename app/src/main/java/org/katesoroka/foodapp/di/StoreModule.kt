package org.katesoroka.foodapp.di

import android.content.Context
import androidx.room.Room
import org.katesoroka.foodapp.data.database.AppDatabase
import org.katesoroka.foodapp.data.repo.FoodRepository
import org.katesoroka.foodapp.data.repo.FoodRepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

const val STORE_NAMESPACE = "store_namespace"

val storeModule = module(override = true) {

    single(STORE_NAMESPACE) { provideDatabase(androidContext()) }

    single(STORE_NAMESPACE) { provideProductDao(get(STORE_NAMESPACE)) }

    single<FoodRepository> { FoodRepositoryImpl(get(NETWORK_NAMESPACE), get(STORE_NAMESPACE)) }

}

fun provideDatabase(context: Context): AppDatabase {
    return Room.databaseBuilder(context, AppDatabase::class.java, AppDatabase.DATABASE_NAME)
        .build()
}

fun provideProductDao(database: AppDatabase) = database.productDao()