package org.katesoroka.foodapp.di

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.katesoroka.foodapp.BuildConfig
import org.katesoroka.foodapp.data.api.FoodApi
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

const val NETWORK_NAMESPACE = "default_namespace"

val networkModule = module {

    single(NETWORK_NAMESPACE) { provideOkHttpClient() }

    single(NETWORK_NAMESPACE) { provideRetrofit(get(NETWORK_NAMESPACE)) }

    single(NETWORK_NAMESPACE) { provideFoodService(get(NETWORK_NAMESPACE)) }

//    single { provideFoodService<FoodApi>(get(), getProperty(BuildConfig.SERVER_BASE_URL)) }

}

fun provideOkHttpClient(): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
    return OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor)
        .build()
}

fun provideRetrofit(client: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.SERVER_BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

fun provideFoodService(retrofit: Retrofit): FoodApi = retrofit.create(FoodApi::class.java)

//inline fun <reified T> provideFoodService(okHttpClient: OkHttpClient, url: String): T {
//    val retrofit = Retrofit.Builder()
//        .baseUrl(url)
//        .client(okHttpClient)
//        .addConverterFactory(GsonConverterFactory.create())
//        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//        .build()
//    return retrofit.create(T::class.java)
//}