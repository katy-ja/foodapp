package org.katesoroka.foodapp.view.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_main.*
import org.katesoroka.foodapp.R
import org.koin.android.viewmodel.ext.android.viewModel
import androidx.recyclerview.widget.GridLayoutManager

class MainFragment : Fragment() {

    companion object {

        private const val NUMBER_OF_COLUMNS = 2

    }

    private val viewModel: MainViewModel by viewModel()

    private var adapter: FoodRecyclerViewAdapter? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeViewModel()
        setupRecyclerView()
        swipeRefreshLayout.setOnRefreshListener {
            updateProducts()
        }
        updateProducts()
    }

    override fun onDestroyView() {
        removeObservers()

        super.onDestroyView()
    }

    private fun observeViewModel() {
        viewModel.foodListLiveData.observe(this, Observer { products ->
            updateAdapter()

            if (products != null && products.isNotEmpty()) {
                emptyPlaceholderView.visibility = View.GONE
                foodListView.visibility = View.VISIBLE
            } else {
                emptyPlaceholderView.visibility = View.VISIBLE
                foodListView.visibility = View.GONE
            }
        })
        viewModel.refreshLiveData.observe(this, Observer { isRefresh ->
            swipeRefreshLayout.isRefreshing = isRefresh
        })
    }

    private fun setupRecyclerView() {
        foodListView.layoutManager = GridLayoutManager(context, NUMBER_OF_COLUMNS)
        foodListView.adapter = getAdapter()
    }

    private fun removeObservers() {
        viewModel.foodListLiveData.removeObservers(this)
        viewModel.refreshLiveData.removeObservers(this)
    }

    private fun getAdapter(): FoodRecyclerViewAdapter {
        return adapter?.let {
            it
        } ?: run {
            val adapter = FoodRecyclerViewAdapter()
            this.adapter = adapter
            adapter
        }
    }

    private fun updateAdapter() {
        adapter?.updateProducts(viewModel.foodListLiveData.value)
    }

    private fun updateProducts() {
        viewModel.refreshLiveData.value = true
        viewModel.updateProducts()
    }

}
