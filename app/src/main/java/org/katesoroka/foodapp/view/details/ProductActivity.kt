package org.katesoroka.foodapp.view.details

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_product.*
import kotlinx.android.synthetic.main.activity_product.swipeRefreshLayout
import kotlinx.android.synthetic.main.fragment_main.*
import org.katesoroka.foodapp.R
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ProductActivity : AppCompatActivity() {

    private var productId: String? = null

    private val viewModel: ProductViewModel by viewModel { parametersOf(productId) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_product)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val bundle = intent.extras ?: run {
            Toast.makeText(this, R.string.activity_product_toast_no_item, Toast.LENGTH_SHORT)
                .show()
            return
        }

        productId = ProductActivityArgs.fromBundle(bundle).productId

        observeViewModel()
        swipeRefreshLayout.setOnRefreshListener {
            updateProduct()
        }
        updateProduct()
    }

    override fun onDestroy() {
        super.onDestroy()

        removeObservers()
    }

    private fun observeViewModel() {
        viewModel.productLiveData.observe(this, Observer { product ->
            Picasso.get()
                .load(if (product.imageUrl.isEmpty()) null else product.imageUrl)
                .placeholder(R.drawable.ic_cloud_off)
                .fit()
                .centerInside()
                .into(toolbarImage)
            collapsingToolbar.title = product.name
            id.text = product.id
            price.text = product.price.toString()
            description.text = product.description
        })
        viewModel.refreshLiveData.observe(this, Observer { isRefresh ->
            swipeRefreshLayout.isRefreshing = isRefresh
        })
    }

    private fun removeObservers() {
        viewModel.productLiveData.removeObservers(this)
        viewModel.refreshLiveData.removeObservers(this)
    }

    private fun updateProduct() {
        viewModel.updateProduct()
    }

}
