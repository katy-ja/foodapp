package org.katesoroka.foodapp.view.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import org.katesoroka.foodapp.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
    }

}
