package org.katesoroka.foodapp.view.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.katesoroka.foodapp.data.model.FoodFull
import org.katesoroka.foodapp.data.repo.FoodRepository

class ProductViewModel(private val productId: String,
                       private val foodRepository: FoodRepository) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val productLiveData: MutableLiveData<FoodFull> = MutableLiveData()

    val refreshLiveData = MutableLiveData<Boolean>()

    fun updateProduct() {
        compositeDisposable.clear()
        val disposable = foodRepository.getProduct(productId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { product ->
                    productLiveData.postValue(product)
                    refreshLiveData.postValue(false)
                },
                // todo show some notification or toast about error
                onError =  { refreshLiveData.postValue(false) }
            )
        disposable.addTo(compositeDisposable)
    }

    override fun onCleared() {
        super.onCleared()

        compositeDisposable.dispose()
    }

}
