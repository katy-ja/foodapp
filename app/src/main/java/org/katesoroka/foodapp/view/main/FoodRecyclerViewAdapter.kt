package org.katesoroka.foodapp.view.main

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import androidx.navigation.Navigation
import org.katesoroka.foodapp.R
import org.katesoroka.foodapp.data.model.Food
import org.katesoroka.foodapp.view.details.ProductActivityArgs

class FoodRecyclerViewAdapter(private var values: List<Food>? = null) :
        RecyclerView.Adapter<FoodViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder {
        return FoodViewHolder(parent)
    }

    override fun onBindViewHolder(holderItem: FoodViewHolder, position: Int) {
        val food = values?.get(position) ?: return
        holderItem.bind(food)

        with(holderItem.itemView) {
            tag = food
            val bundle = ProductActivityArgs(food.id).toBundle()
            setOnClickListener(
                Navigation.createNavigateOnClickListener(R.id.action_mainFragment_to_productActivity, bundle))
        }
    }

    override fun getItemCount() = values?.size ?: 0

    fun updateProducts(values: List<Food>?) {
        this.values = values
        notifyDataSetChanged()
    }
}
