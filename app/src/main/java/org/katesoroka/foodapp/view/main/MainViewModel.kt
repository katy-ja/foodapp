package org.katesoroka.foodapp.view.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.katesoroka.foodapp.data.model.Food
import org.katesoroka.foodapp.data.repo.FoodRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class MainViewModel(private val foodRepository: FoodRepository) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val refreshLiveData = MutableLiveData<Boolean>()

    val foodListLiveData = MutableLiveData<List<Food>>()

    fun updateProducts() {
        compositeDisposable.clear()
        val disposable = foodRepository.getProducts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onNext = { products ->
                        foodListLiveData.postValue(products)
                        refreshLiveData.postValue(false)
                    },
                    // todo show some notification or toast about error
                    onError =  { refreshLiveData.postValue(false) }
                )
        disposable.addTo(compositeDisposable)
    }

    override fun onCleared() {
        super.onCleared()

        compositeDisposable.dispose()
    }

}
