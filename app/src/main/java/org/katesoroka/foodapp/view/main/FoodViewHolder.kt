package org.katesoroka.foodapp.view.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import org.katesoroka.foodapp.R
import org.katesoroka.foodapp.data.model.Food
import org.katesoroka.foodapp.databinding.ListItemContentBinding

class FoodViewHolder(parent: ViewGroup,
                     private val binding: ListItemContentBinding =
                         ListItemContentBinding.inflate(
                                         LayoutInflater.from(parent.context),
                                         parent,
                                         false))
    : RecyclerView.ViewHolder(binding.root) {

    fun bind(food: Food) {
        with(binding) {
            Picasso.get()
                .load(if (food.imageUrl.isEmpty()) null else food.imageUrl)
                .placeholder(R.drawable.ic_cloud_off)
                .fit()
                .centerCrop()
                .into(imageView)
            name.text = food.name
            price.text = food.price.toString()
        }
    }

}